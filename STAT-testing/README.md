#STAT TESTING

file: scr_makemax.sh

Receives an input file of the *.observation* type (created by *../feature-training/scr_gentrain.sh*) and creates a *.rangemax_uniqbbl* file which contains all cbbt-phases and their respective maximum value of unique bbl types observed during training of the input file. 

Example: `./scr_makemax.sh qsort.observation qsort.rangemax_uniqbbl` receives the qsort.observation file with cbbt-phase features trained for all training inputs, in the order which they were executed, and returns the maximum value of unique bbl types for each cbbt-phase in the qsort.rangemax_uniqbbl file.

________________

file: fast_checkermax.cpp -> fastcheckermax

checks whether the phase's number of unique BBLs in each phase recorded in the .pfs files is higher than the recorded max.
It requires/reads the output from **scr_makemax.sh**, $app_behavior_rangemax_uniqbbl.
It also requires a trace prefix and trace suffix, to create an expression to catch numbered traces.
Its two final parameter are integers to select the traces.

Example: `fastcheckermax qsort.rangemax_uniqbbl qsort .bbl 1 12` will check whether any traces from qsort1.bbl to qsort12.bbl contains a cbbt-phase with more unique bbl types than observed
in the qsort.rangemax_uniqbbl file.

__________________

file: fast_checkermaxphases.cpp -> fastcheckermaxphases

checks whether the phase's number of unique BBLs is higher than the recorded max AND whether the transition between this phase and the next phase also occurs in the expected behavior for this phase.
It requires/reads the $app.behavior file to parse possible CBBT-phase sequences, as well as $app_behavior_rangemax_uniqbbl.
It also requires a trace prefix and trace suffix, to create an expression to catch numbered traces.
Its two final parameter are integers to select the traces.

Example: `fastcheckermaxphases qsort.behavior qsort.rangemax_uniqbbl qsort .bbl 1 12` will check whether any traces from qsort1.bbl to qsort12.bbl contains a cbbt-phase with more unique bbl types than observed in the qsort.rangemax_uniqbbl file and if the sequence of that cbbt-phase and the upcoming cbbt has been observed in the qsort.behavior file.

