/* fastchecker_max.cpp 
 * inputs: 
 *      - rangemax_uniqbbl file name (for the application)
 *      - trace prefix name (for the cbbt traces of the application)
 *	- trace suffix name (usually .pfs, i.e. the phase features)
 *	- trace suffix start range (for the cbbt traces of the application)
 *      - trace suffix final range (for the cbbt traces of the application)
 * output:
 *      - stdout, prints whenever an anomaly occurs
 *
 *      This code checks, for all traces (ending .pfs) in the suffix range (an integer number), whether the number of unique BBLs for each phase observation exceeds the maximum observed in the behavior file. If it does not, increase the counter and prints a message reporting the anomaly.
 *      This will count the number of false positive phases for correct execution traces, and the number of anomalous phases in malicious execution/exploit traces.
 *	
 *      Example: ./fastchecker_max qsort.rangemax_uniqbbl qsorttrace 10
 *       Reads qsort.behavior into a behavior table, and then reads qsorttrace1.pfs, qsorttrace2.pfs, qsorttrace3.pfs, ..., qsorttrace10.pfs
 *       to look for number of unique bbls larger than stored in behavior table.
 * */

#include<cstdio>
#include<cstdlib>
#include<map>
#include<set>
#include<utility>
#include<string>
#include<fstream>
#include<iostream>
#include<sstream>

using namespace std;

//maps a CBBT pair (bbl source, bbl dest) and the largest number of unique bbls found in the training set for that CBBT phase
std::map<std::pair < long,long >, int > table;

//vars used in the iterations to read numbers from files
long source,dest = 0;
int count = 0;
int types = 0;

//argument vars
std::string  rangemax_uniqbblfile_name;
std::string  traceprefix_name;
std::string tracesuffix_name;
int tracesuffix_finalrange = 0;
int tracesuffix_startrange = 0;
//------------------------------------------------
//read behavior table function into map
//------------------------------------------------
int readtable(){
	ifstream tablefile;
	tablefile.open(rangemax_uniqbblfile_name.c_str());
	
	if (!tablefile.is_open()){
		cout << "problem opening tablefile" << endl;
		exit(1);
	}
	while ((tablefile >> source >> dest >> types)){
		table[make_pair(source,dest)] = types;	
	}

	tablefile.close();
	return 0;
}
//------------------------------------------------

int main(int argc, char ** argv ){

	if (argc != 6){
		printf("expects rangemax_uniqbbl file name, trace prefix name, trace suffix name, trace suffix start range, and trace suffix final range\n");
		exit(1);
	}

	rangemax_uniqbblfile_name = argv[1];
	traceprefix_name = argv[2];
	tracesuffix_name = argv[3];
	tracesuffix_startrange = atoi(argv[4]);
	tracesuffix_finalrange = atoi(argv[5]);

	readtable();

	//count how many times the number of unique bbl types was larger than the value found in table for that cbbt phase
	int anomalies = 0;
	int anomfiles = 0;
	//for each tracefile
	for (int i = tracesuffix_startrange; i<=tracesuffix_finalrange; i++){
		bool anon = false;
		//open file
		std::ostringstream ostr;
		ostr << i;
		
		string inputname = traceprefix_name + ostr.str() + tracesuffix_name;

		ifstream procfile;
		procfile.open(inputname.c_str());	
	
		if (!procfile.is_open()){
			cerr << "problem with procfile, can't open " << inputname << endl;
			break;
		}

		//read each line and look whether the number of bbl types appears in the table
		while ((procfile >> source >> dest >> count >> types)){
			if (types > table[make_pair(source,dest)]){
				cout << "found anomaly in instance of phase " << source << "-" << dest << ", number of unique bbl types " << types << " is larger than observed during training of cbbt phase set, " << table[make_pair(source,dest)] << endl;
				anomalies++;
				anon = true;	
			}	
		}	
		procfile.close();
		if (anon)
			anomfiles++;
	}
	cout << "total anomalies = " << anomalies << endl;
	cout << "total anomalous files = " << anomfiles << endl;
	return 0;
}
