#!/bin/bash

#expects app to be a behavior file

app=$1
outf=$2

cut -f 1,2,4 -d ' ' ${app} | sort | uniq > ${app}.uniq_values
cut -f 1,2 -d ' ' ${app} | sort | uniq > ${app}.phases

while IFS='' read -r line || [[ -n "$line" ]]; do	
	phase=$(echo $line | awk '{print $1,$2}')
	max=$(grep "$phase" ${app}.uniq_values | awk '{print $3}' | sort -n -r | head -n 1)
	echo "$phase $max" >> $outf
done < ${app}.phases

rm ${app}.uniq_values
rm ${app}.phases
