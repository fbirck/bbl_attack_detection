/*fastchecker_maxphases.cpp 
 * inputs: 
 *	- behavior file name, containing sequences of CBBT phases in all training traces 
 *      - rangemax_uniqbbl file name (rangemax_uniqbbl generated by scr_makemax.sh for the application)
 *      - trace prefix name (for the cbbt traces of the application)
 *	- trace suffix name (usually .pfs, the phase features)
 *	- trace suffix start range (for the cbbt traces of the application)
 *      - trace suffix final range (for the cbbt traces of the application)
 * output:
 *      - stdout, prints whenever an anomaly occurs
 *
 *      This code checks, for all traces in the suffix range, whether a given phase and its number of unique BBLs occur in the behavior file. If it does not, increase the counter and prints a message reporting the anomaly.
 *      This will count the number of false positive phases for correct execution traces, and the number of anomalous phases in malicious execution/exploit traces.
 *	
 *	Example:
 *	./fastchecker_maxphases qsort.rangemax_uniqbbl behaviors/qsortbehaviors/qsort qsorttrace 10
 *
 *	The program will load qsort.rangemax_uniqbbl into table, and will load all phase sequences for each file in tableseqphase. 
 *	For each trace qsorttrace1, qsorttrace2, ..., qsorttrace10 it will register an anomaly whenever a phase P1 has more unique bbls than the maximum registed in the table for P1 instances
 *	AND its execution is followed by a phase P2 such that the sequence P1-P2 is not observed in P1's phase sequence file.
 * */

#include<cstdio>
#include<cstdlib>
#include<map>
#include<set>
#include<utility>
#include<sstream>
#include<string>
#include<fstream>
#include<iostream>

using namespace std;

//maps a CBBT phase (defined by a pair BBL source - BBL dest) to an integer representing the largest number of Unique BBL types observed during training in the CBBT phase
std::map<std::pair < long,long >, int > table;

//maps a CBBT phase (defined by a pair BBL source - BBL dest) to a set of CBBT phases which were observed to follow the primary key CBBT phase during training
std::map<std::pair < long,long >, std::set<pair < long, long > > > tableseqphase;

//vars used in the iterations to read numbers from files
unsigned long source,prevsource,dest,prevdest = 0;
int count,prevcount,types,prevtypes = 0;

string behaviorfile_name;
string rangemaxfile_name;
string traceprefix_name;
string tracesuffix_name;
int tracesuffix_startrange = 0;
int tracesuffix_finalrange = 0;

//function to read a map of CBBT-types and another map of CBBT-> set of follow-up CBBTs
int readtables(){
	
	ifstream infile;

	//read maximum values for each CBBT-defined phase
	infile.open(rangemaxfile_name.c_str());
	if (!infile.is_open()){
		cout << " problem opening rangemaxuniqbbl file" << rangemaxfile_name << endl;
		return 1;
	}

	while ((infile >> source >> dest >> types)){
		table[make_pair(source,dest)] = types;	
	}	
	
	infile.close();	

	//read set of phaseseqs from the full behavior file
	infile.open(behaviorfile_name.c_str());
	if (!infile.is_open()){
		cout << " problem with infile opening behavior file" << behaviorfile_name << endl;
		return 1;
	}
	
	while ((infile >> source >> dest >> count >> types)){
        	tableseqphase[make_pair(prevsource,prevdest)].insert(make_pair(source,dest));
		prevsource = source;
		prevdest = dest;	
	}

	infile.close();
	cout << "PHASES and number of seqphases" << endl;
	for (auto it = tableseqphase.begin(); it != tableseqphase.end(); it++){
		cout << "phase " << std::to_string(it->first.first) << " to " << std::to_string(it->first.second) << " has " << std::to_string(it->second.size()) << " phases " << endl;
	}
	return 0;
}

int main(int argc, char ** argv ){

	if (argc != 7){
		cout << "expects behavior file name, rangemax_uniqbbl file name, trace prefix name, trace suffix name, trace suffix start range and trace suffix final range" << endl;
		return 1; 
	}

	behaviorfile_name = argv[1];
	rangemaxfile_name = argv[2];
	traceprefix_name = argv[3];
	tracesuffix_name = argv[4];
	tracesuffix_startrange = atoi(argv[5]);
	tracesuffix_finalrange = atoi(argv[6]);

	readtables();	

	int anomalies = 0;
	int anomalousfiles = 0;
	int succfiles = 0;
	//for each tracefile
	for (int i = tracesuffix_startrange; i<=tracesuffix_finalrange; i++){
		bool anon = false;
		//open file
		stringstream ostr;
		ostr << i;
		string inputname = traceprefix_name + ostr.str() + tracesuffix_name;
		ifstream procfile;
		procfile.open(inputname.c_str());
		if (!procfile.is_open()){
			cout << "problem opening procfile" << inputname << endl;
			continue; //ignore this file
			//break;
		}

		//first phase, has no prev
		if (!(procfile >> prevsource >> prevdest >> prevcount >> prevtypes)){
			cout << "error reading first line of procfile" << inputname << endl;
			continue; //ignore this file
		}
		//read each line and look whether the number of bbl types appears in the table
		while ((procfile >> source >> dest >> count >> types)){
			//if types is larger than what previously seen
			if ((prevtypes -1 ) > table[make_pair(prevsource,prevdest)] ){
				//and if the sequence is not in the set of previously seen behaviors
				if (tableseqphase[make_pair(prevsource,prevdest)].find(make_pair(source,dest)) == tableseqphase[make_pair(prevsource,prevdest)].end()){
					cout << "found anomaly in file " << inputname <<", bbl types " << prevtypes << "  and phase seq " << source << "-" << dest << " not found in cbbt phase set" << endl;
					anomalies++;
					anon = true;
				}	
			}
		
			//reassign variables to keep track of who is the currently processed phase and who is the following phase
			prevsource = source;
			prevdest = dest;
			prevtypes = types;
			prevcount = count;
		}	
		procfile.close();
		if (anon)
			anomalousfiles++;
		succfiles++;
	}

	cout << "succesful files = " << succfiles << endl;
	cout << "total anomalies = " << anomalies << endl;
	cout << "total anomalous files = " << anomalousfiles << endl;
	return 0;
}
