/*
 * CBBT_generate.cpp 
 * Input:
 * - BBL trace file
 * Output:
 * - Raw CBBTs file
 *
 *   This file scans the BBL trace of an application execution to find all CBBTs in accordance to Ratanaworabhan et al. "Program Phase Detection based on Critical Basic Block Transitions". It creates an infinite BBL cache and records the transitions between BBLs in the cache (i.e. repetitive behavior in pre-existing phases) and BBLs outside the cache (i.e. new behavior in a new phase). We keep track of first occurence, last occurence, frequence, signature, and instruction count of each CBBT and its signature for the filtering process in cbbt_process.py 
 * */


#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <map>
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

//structure to represent each cbbt and its information
struct cbbtinfo {
	std::vector<long unsigned> signature;
	long unsigned fstamp;
	long unsigned lstamp;
	long unsigned freq;
	long unsigned instcount;
	};

std::map<long unsigned, int> bblcache; //infinite cache representation using map

bool hitting = false; //var to keep account of whether last bbl was on cache or not
long unsigned full_instcount = 0; //full instruction counter
long unsigned instcount = 0; //cbbt instruction counter
std::vector<long unsigned> transsig; //vector containing bbls that miss on cache and represent a cbbt signature
std::vector<cbbtinfo> cbbtvec; //vector containing all cbbts

int main (int argc,char *argv[]){

	//basic usage check
	if (argc != 3){
		cout << "wrong number of arguments, expects 2 arguments: input file name, and output file name" << endl;
		exit(1);
	}
	//file name receiving	
	string inputfile = argv[1];
	string outputfile = argv[2];
	long unsigned prevbbladdr = 0; //keeps track of previous bbl, to make relationships between bbls
	long unsigned bbladdr = 0; //current bbl 
	long unsigned numins = 0; //number of instructions in bbl
	
	std::ifstream infile;
        infile.open(inputfile.c_str());

	while(infile >> bbladdr >> numins){
		full_instcount+= numins;
		std::map<long unsigned, int>::iterator itv = bblcache.find(bbladdr); //iterator for cache
		//if BBL not in cache
		if (itv == bblcache.end()){
			bblcache[bbladdr] = 1;
			
			//if previous bbl was a hit, this is a critical transition
			if (hitting)
				transsig.push_back(prevbbladdr); //push back last "hitting" bbl to mark the source bbl of a CBBT
			transsig.push_back(bbladdr); //push back this bbl to current signature being constructed
			instcount += numins; //add to CBBT's length in instructions
			hitting = false; //update var status, as this was a miss
		}
		else{//if it is in cache
		
			//if previous BBL was a miss, we have finished recording a CBBT signature, and must wrap up the CBBT
			if (!hitting){
				//create CBBT struct with the collected info
				cbbtinfo insert = cbbtinfo();
				insert.fstamp = full_instcount;
				insert.lstamp = full_instcount;
				insert.freq = 1;
				insert.instcount = instcount; 
				insert.signature = transsig;
				
				//insert into vector of cbbts and reset vars
				cbbtvec.push_back(insert); 
				instcount = 0;
				transsig.clear();
			}
			
			//update vars
			bblcache[bbladdr] += 1;
			hitting = true;
			
			//search to check if this transition is an already recorded CBBT. If it is, update freq and last stamp values
			for (unsigned i = 0; i < cbbtvec.size(); i++){
				if ((prevbbladdr == cbbtvec[i].signature[0]) && (bbladdr == cbbtvec[i].signature[1])){
					cbbtvec[i].freq += 1;
					cbbtvec[i].lstamp = full_instcount;
				}
			}
		}
		//update var for next iteration
		prevbbladdr = bbladdr;
	}
	
	//finished reading	
	infile.close();

	//open output file
	std::ofstream outfile;
        outfile.open(outputfile.c_str());

	//for each CBBT in the vector, print its info to the output file.
	for (unsigned i = 0; i < cbbtvec.size(); i++){
		long unsigned freqsum = 0;
		//for each BBL in the signature, print it to the line
		for (unsigned j = 0; j < cbbtvec[i].signature.size(); j++){
			outfile << cbbtvec[i].signature[j] << " ";
			freqsum += bblcache[cbbtvec[i].signature[j]];
		}
		outfile << "freqsum is " << freqsum << ", inst is " << cbbtvec[i].instcount << ", first stamp is " << cbbtvec[i].fstamp;
	        outfile	<< ", last stamp is " << cbbtvec[i].lstamp << ", cbbt freq is " << cbbtvec[i].freq << endl; 

	}
	//close and return
	outfile.close();
	return 0;
}

