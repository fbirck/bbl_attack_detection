import sys

filteropt = sys.argv[1]
nofilter = True

if filteropt == "nofilter": #we can ignore the rest of the options
	#expects nofilteropt, input trace file name, output file name
	if (len(sys.argv) != 4):
		print "wrong number of arguments, expects either:"
		print "\"nofilter\" \"input file\" \"output file\""
		print "or"
		print "\"filter\" \"input file\" \"output file\" \"text offset\" \"text size\" and \"phase granularity\""
		exit()
	inputfile = sys.argv[2]
	outputfile = sys.argv[3]
	nofilter = True
else:
	#expects filteropt, input trace file name, output file name, text offset, text size, and phase granularity.
	if (len(sys.argv) != 7):
		print "wrong number of arguments, expects either:"
		print "\"nofilter\" \"input file\" \"output file\""
		print "or"
		print "\"filter\" \"input file\" \"output file\" \"text offset\" \"text size\" and \"phase granularity\""
		exit()

	inputfile = sys.argv[2]
	outputfile = sys.argv[3]
	bottomlimit = int(sys.argv[4]) #bottom limit of .text section
	uplimit = int(sys.argv[5]) + bottomlimit #upper limit of .text section
	phaseg = int(sys.argv[6]) #phase granularity
	nofilter = False


print("got the following args")
print("bottom limit = " + str(bottomlimit))
print("upper limit = " + str(uplimit))
print("phaseg = " + str(phaseg))

#i read from file into strings per element, split by comma
lines = []
#with open("/home/fbmoreira/CacheSim-master/maintrace_31548.out") as file:
with open(inputfile) as file:
	for line in file:
		lines.append(line.strip().split(','))

#turn each field of those strings into one list element
proclines = []
for i in lines:
	for j in i:
		proclines.append(j.strip().split())	


nmat = []
j=0

#proclines has all
#iterate through all fields
index = 0
while ( index != len(proclines)):
	nl = []
	i = proclines[index]
	aux =i.pop()
	i.pop()
	i.pop()
	nl.append(i) #add signature
	nl.append(aux) #add freqsum
	index = index + 1 
	i = proclines[index]
	#	//finished processing signature line 

	nl.append(i.pop()) #instcount
	index = index + 1
	i = proclines[index] 
	nl.append(i.pop()) #timestamp 1
	index = index + 1
	i = proclines[index]
	nl.append(i.pop()) #timestamp last
	index = index + 1 
	i = proclines[index]
	nl.append(i.pop()) #cbbt freq
	index = index + 1
	nmat.append(nl)


print("filtering with nofilter = " + str(nofilter))
#pass through all cbbts and filter them
filtermat = []
for i in nmat:
	#cbbts are 1. in .text, 2. happen only once OR happen with average  phaseg inst between them, 3. signature size must be larger than 2, 4. freqsum > 1000, 5. a non-recurring cbbt first timestamp
	if (nofilter):
		if (len(i[0]) > 2):
			filtermat.append(i)
	elif ((int(i[0][0]) <= uplimit) and (int(i[0][0]) >= bottomlimit)): #inside text, uplimit and bottomlimit must be received as args
		if ((i[3] == i[4]) or ((((int(i[4]) - int(i[3])) / (int(i[5]) - 1)) > phaseg))): #non-recurrent CBBT OR period is larger than phase
			if (len(i[0]) > 2):				#signature larger than the transition only
				print("entered here to add")
                                filtermat.append(i)


fstampl = [i[3] for i in filtermat] #obtain fstamps of all cbbts in order of appearance
wout = open(outputfile, "w")
fsli = 1

#somewhat unused code, was aiming to mix phases, but for now no difference between CBBT-CBBT period smaller or larger than phase granularity.
#all conditions do the same, print cbbt out. To be changed in the future.
for i in filtermat:
	if fsli < len(fstampl): #if inside bounds of list of fstamps
		if (i[3] == i[4]): #i fstamp == lstamp
			#if (int(fstampl[fsli]) - int(i[3]) > phaseg): #if difference between first reference to phase and this reference is larger than chosen phaseg
			for j in i[0]:
				wout.write(str(j) + " ")
			wout.write("\n")
			for k in range(1,len(i)):
				wout.write(str(i[k]) + " ")
			wout.write("\n")
		else:
			for j in i[0]:
				wout.write(str(j) + " ")
			wout.write("\n")
			for k in range(1,len(i)):
				wout.write(str(i[k]) + " ")
			wout.write("\n")
			
	else:
		for j in i[0]:
			wout.write(str(j) + " ")
		wout.write("\n")
		for k in range(1,len(i)):
			wout.write(str(i[k]) + " ")
		wout.write("\n")
	fsli += 1	
wout.close()
