#!/bin/bash

filter=$1
prefix=$2 #prefix of the files containing the application's cbbts. The script expects multiple files since you should execute multiple times to increase code coverage

#creates a single file with all cbbts for the application
cat ${prefix}*.cbbts >> all_cbbts
sort -n all_cbbts | uniq > ${prefix}_unique_sorted 
rm all_cbbts
if [ "$filter" != "nofilter" ]; then
	binary=$3
	p=$4 #granularity of phases as discussed in the paper. Should be a natural number, preferably larger than 1000
	aux=$(readelf -S ${binary} | grep text -A 1 | awk '{print $4}' | head -n 1)
	offset=$((16#${aux})) #.text offset address. 
	aux=$(readelf -S ${binary} | grep text -A 1 | awk '{print $1}' | tail -n 1)
	tsize=$((16#${aux})) #.text size.	
	python cbbtprocess.py $filter ${prefix}_unique_sorted ${prefix}.filteredcbbts $offset $tsize $p 
else
	python cbbtprocess.py $filter ${prefix}_unique_sorted ${prefix}.filteredcbbts 
fi;

#rm ${prefix}_unique_sorted
