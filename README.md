# BBL_Attack_Detection

Repository with implementation of critical basic block transition methodology. 
Implemented to make phase features explicit, which are used to detect attacks.
Run a "make clean; make" to compile all necessary binaries in your system.

_______________________________________________

Each subfolder in these steps has an additional README explaining its tools.
The basic chain of usage is as follows:

1. Generate a basic block (BBL) trace of target application using the Pin tool in folder ***BBL-Sim***. 

Example: `cd /home/aizen/bbl_attack_detection/BBL-sim ; ./runbbltrace.sh ls`. This will generate a trace_ls_#pid.bbl file

2. Create all critical basic block transitions using ***CBBT-process/cbbt_generate***, with the training basic block trace (or multiple traces using scr_batch_cbbgenerate.sh) as input. 

Example: `cd /home/aizen/bbl_attack_detection/CBBT-process; ./cbbtgenerate ../BBL-sim/trace_ls_#pid.bbl ls.cbbt`
This will generate a cbbt file (ls.cbbt) using the single trace_ls_#pid.bbl file we created in the previous example.

Example: `cd /home/aizen/bbl_attack_detection/CBBT-process; ./scr_batch_cbbtgenerate ../BBL-sim/trace_ls_*.bbl ls.cbbt`
This will generate a cbbt file (ls.cbbt) using all traces fitting the glob expression ../BBL-sim/trace_ls_*.bbl.
Thus, we can create better CBBTs by taking into account more varied executions of an application, reducing false positives.

3. Filter the relevant critical basic block transitions using the ***CBBT-process/cbbt_process.py*** script, receives the output from ***cbbtgenerate***.

Example(1):`python cbbtprocess.py filter ls.cbbt ls.filteredcbbts 422706 600 10000` filters file ls.cbbt into ls.filteredcbbts, with .text addresses ranging from 422706 to 422706+600, and cbbt granularity 10000

Example(2):`python cbbtprocess.py nofilter ls.cbbt ls.filteredcbbts` does not apply any filter, but is necessary to format cbbts for the next step.

4. With the output from ***cbbtprocess.py*** and a target basic block trace generated using Pin tool, use feature_training/bbl_simulate with both as input.
	
	This will generate a trace of phase observations, with the structure of: 
	
	"CBBT-id" "Number of BBL executions" "Number of Unique BBLs in Phase" 

	These features may be changed by changing the ***bbl_simulate.cpp*** source code and recompiling.
	Advanced usage to detect exploits/anomalies using our methodology (which you can change and experiment as you wish) are as follows:
	
	1. Use ***feature-training/scr_gentrain.sh*** to create a *.behavior* file, determining what is the common behavior of cbbt-phases. First parameter is a filtered cbbts file. Second parameter is output file. Third parameter is prefix of trace files. Fourth parameter is suffix of trace files. The script will use all traces in the glob $prefix+(0-9)$suffix to generate a unified common behavior file in the defined output.

	Example: `cd /home/aizen/bbl_attack_detection/feature_training; ./scr_gentrain.sh ../CBBT-process/ls.filteredcbbts ls.behavior "../BBL-sim/trace_ls_" ".bbl"`

	2. Use ***feature-training/scr_genpfs.sh*** to create *.pfs* files, containing phase features for cbbt-phases of traces which you want to test for anomalies in comparison to the behavior trained in step **5**. This script is nearly identical to scr_gentrain.sh, but it has only three parameters, without an output file. It generates multiple files, one per trace, replacing the ".bbl" suffix of the input trace by a ".pfs" suffix.
	This script should be used to transform new BBL traces (attacks, and new regular executions) into CBBT phase features, so we can analyze whether these features behave normally.	

	Example: `cd /home/aizen/bbl_attack_detection/feature_training; ./scr_genpfs.sh ../CBBT-process/ls.filteredcbbts "../BBL-sim/trace_ls_" ".bbl"`	

5. Use ***STAT-testing*** tools to find anomalies:

	1. Use ***STAT-testing/scr_makemax.sh*** to create a *.rangemax_uniqbbl* file from the *.behavior* file of step **4.1** to run our analysis. First parameter is the behavior file, second paramter is output file. 

	Example: `cd /home/aizen/bbl_attack_detection/STAT-testing; ./scr_makemax.sh ../feature-training/ls.behavior ls.rangemax_uniqbbl` 

	2. Use ***STAT-testing/fastcheckermax*** to test whether cbbt-phases in the test set have number of unique bbl types above the threshold observed in the *.rangemax_uniqbbl* file. First parameter is rangemax_uniqbbl file, second parameter is phase feature (.pfs) files prefix, third parameter is phase feature (.pfs) files suffix, fourth parameter is initial range of trace number, fifth parameter is final number.
	This script assumes that all traces are numbered, in the form of $prefix${n}$suffix, where ${n} is the trace number. 
	
	Example: `cd /home/aizen/bbl_attack_detection/STAT-testing; ./fastcheckermax ls.rangemax_uniqbbl ../BBL-sim/ls_ ".pfs" 1 10`
	This will analyze pfs traces "../BBL-sim/ls_1.pfs", "../BBL-sim/ls_2.pfs", ..., "../BBL-sim/ls_10.pfs" searching for instances where the number of bbl types phase feature was larger than what found during training.

	2. Use ***STAT-testing/fastcheckermaxphases*** to test whether cbbt-phases in the test set have number of unique bbl types above the threshold observed in the *.rangemax_uniqbbl* file and a sequence of cbbt-phases which does not appear in the *.behavior* file. We have found this analysis to catch the exploits tested so far with 0 false positives. We have found less than 1% false positives running other applications, although we are still expanding our tests. This seems highly dependent on the granularity ***p*** used during cbbt filtering in step **3**. 

	Example: `cd /home/aizen/bbl_attack_detection/STAT-testing; ./fastcheckermaxphases ../CBBT-process/ls.behavior ls.rangemax_uniqbbl ../BBL-sim/ls_ ".pfs" 1 10`
	
	This is similar to the previous step, but will also use the behavior file as an additional parameter to check for anomalous phase sequences which do not show in the behavior file. 


________________________________________________

#Reproducing SPADA:

To reproduce our work in SPADA, we make available the virtual machine "hbvm" used for our tests (Ubuntu14.04-SPADA_reproduction.ova).
It can be downloaded using the following address: https://drive.google.com/open?id=1aO8MYY1Lw93Gd5XbDad-HIhXPSKMk9hH
We used VirtualBox to import the appliance.
This folder (sans the .ova file) ***MUST*** be copied to the virtual machine.
The virtual machine contains both vulnerabilities we used to test, Heartbleed and Shellshock (and likely many more).
It has a single user, **aizen**, whose password is **onetwo345**.

IMPORTANT: We were not able to change some configuration which makes nginx take over tty0.
Thus, make sure to press `alt+F2` to switch to another terminal so you can use the virtual machine.

##Testing Heartbleed:

First, stop the nginx server by running `sudo nginx -s quit`.
Now the server can be restarted when we want.

To trace the executions, go to the BBL-sim folder (/home/aizen/bbl_attack_detection/BBL-sim) and execute `sudo ./runbbltrace.sh nginx`.

Now, from another machine, you can access the server machine, hbvm, with a browser (i.e. insert into your browser https://hbvm/index.html).

You might have to change the /etc/nginx/conf archive to set the external name accordingly to your institution and network. 
If there are any problems with the name, you can simply run `ifconfig` in the VM, obtain the machine IP, and access ***https://xx.xx.xx.xx/index.html***, properly replacing the xx fields with hbvm's IP.
The README inside the VM contains details regarding the structure of the server, so that you can generate normal executions accessing such files.
The server contains the following pages:
* index.html
* index2.html
* images/logo.png
* images/img.jpg

(the last image produces an error on purpose, to trace erroneous paths).

To reproduce the heartbleed exploit, use one of the scripts found in exploits/ with hbvm as the target server.
Example: `python ~/bbl_attack_detection/exploits/pre-handshake-hb.py hbvm`

To finish nginx traces, run `sudo nginx -s quit`. **Do not use nginx -s stop**, as this will prevent pin from finishing up its instrumentation traces properly.


##Testing procmail:

You can run procmail by simply calling `procmail < mailexample` where mailexample is an e-mail in a txt.
The VM comes with plenty of examples in "/home/aizen/mail".

Remember that, to generate traces, one must use `/home/aizen/bbl_attack_detection/BBL-sim/runbbltrace.sh procmail < mail`.
Since procmail generates many calls to formail and bash, you **MUST** ensure that runbbltrace.sh call to pin has the safemode flag activated (-m 1). The default is 0.
Safemode is very slow, so the execution of procmail can take a while.

You can check which e-mails were processed by observing /home/aizen/Procmail/log.
Malicious e-mails may contain a segmentation fault due to improperly finished commands, which is usually printed
at the Procmail log due to output redirection from procmail process calls.

____________________________________________________

Folders:
**BBL-Sim**: Contains a basic block (BBL) tracer implementation using Pin.

**CBBT-Process**: Contains sources, binaries, and scripts to detect (cbbt_generate.cpp) and filter (cbbtprocess.py) the Critical Basic Block Transitions from a BBL trace or set of BBL traces (outputs from *BBL-Sim*).

**exploits**: Contains sources to attack a server replicating heartbleed.

**feature-training**: Contains sources, a binary, and scripts to generate and train cbbt-phase features for bbl traces. 
Uses output from *CBBT-Process* (filtered cbbts) and *BBL-Sim* (bbl traces).

**STAT-testing**: Contains sources, binaries and a script to detect whether a set of cbbt-phase traces (generated by *feature-training/scr_genpfs.sh*) contain anomalies in comparison to a pretrained set of cbbt-phase behavior (generated by *feature-training/scr_gentrain.sh*).

**mibench_bins**: contain binaries of the MiBench benchmark to test the tools.

**mibench_inputs**: contains 20 inputs for each of the MiBench benchmarks to test the tools. Inputs have been created by Grigori Fursin et al.

**mibench_cfg**: contains analysis and comparisons of the control flow graph seen by the BBL traces, the full CFG obtained by dyninst, and the tanimoto distance to compare both graphs.

**dyninst-install**: folder containing Paradyn's dyninst tools. 
We only use it to generate the CFG from the binaries (compiled with static to obtain the full CFG) 

**VtPath-sim**: folder containing VtPath implementation. Needs refactoring to improve readability and usability.
______________________________


All code has commentary, and READMEs are available for each tool.
Create an issue in the bitbucket or send me an e-mail (fbmoreira@inf.ufrgs.br) in case something is missing or if you have suggestions/additions to make.

Happy hacking! (or counter-hacking...)

ps: requires dyninstall for CFG analysis installed in this directory.
