#!/bin/bash

for bench_name in mibench_bins/*; do	
	for fname in CBBT-process/${bench_name##*/}_traces/maintrace*; do 
		out=$(echo ${bench_name##*/}_${fname##*/}_spadaedgelist)
		prev=0;
		while IFS='' read -r line || [[ -n "$line" ]]; do
			next=$(echo $line | awk '{print $1}')	
			echo "$prev $next" | tee -a saida
			prev=$next
		done < $fname
		cat saida | xargs printf "%4s %4s\n" | tr 'a-z' 'A-Z0' | sort -u > out
		lc=$(wc -l out)
		echo $lc | tee -a spada_edge_counts	
		rm saida
	done;
done;	
