#!/bin/bash



for fname in mibench_bins/*; do
	LD_PRELOAD=/home/fbmoreira/bbl_attack_detection/dyninst-install/lib/libparseAPI.so dyninst-install/example $fname > ${fname}_pass1
	grep "\->" ${fname}_pass1 | awk '{ print $1,$2}' > ${fname}_pass2
	sed -i 's/\->//g' ${fname}_pass2
	sed -i 's/\"//g' ${fname}_pass2
	#sed -i 's/ /\r/g' ${fname}_pass2
	tr ' ' '\n' < ${fname}_pass2 > ${fname}_pass3
	cat ${fname}_pass3 | xargs printf "%4s\n" | tr 'a-z ' 'A-Z0' | sort -u > ${fname}_pass4
	lc=$(wc -l ${fname}_pass4)
	echo $lc | tee -a fcfg_counts	
done;	
