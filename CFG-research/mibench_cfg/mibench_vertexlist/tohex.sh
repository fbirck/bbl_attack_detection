#!/bin/bash

for fname in ../../mibench_bins/*; do
	awk '{ printf "%02X\n", $1}' ${fname##*/}_full > ${fname##*/}_hexed &
done;
