#include<stdio.h>

int main(int argc, char ** argv){
	
	
	//treat input
	if (argc != 3){
		printf("ERROR: expects ./tanimoto FileA FileB\n");
		printf("where FileA and FileB are lists of vertices for the Tanimoto coefficiente calculation\n");
		printf("Both files assume numeric, ordered vertex labels for efficient comparison\n");
		return 1;	
	}

	//input graphs
	FILE * inputA;
	FILE * inputB;
	
	//output graph files
	long unsigned sumA = 0;
	long unsigned sumB = 0;
	long unsigned intersection = 0;

	//open all files

	inputA = fopen(argv[1],"r");
	inputB = fopen(argv[2], "r");

	if (inputA == NULL || inputB == NULL){
		printf("invalid file names, or files were not found\n");
		return 1;
	}

	unsigned int A = 0;
	unsigned int B = 0;

	int faval = 0;
	int fbval = 0;
	
	faval = fscanf(inputA,"%x\n", &A);
	fbval = fscanf(inputB,"%x\n", &B);

	if (!faval || !fbval)
		return 1;
	
	//
	while (!feof(inputA) && !feof(inputB)){
		if (!faval){
			printf("reached end of A\n");
			break;
		}
		if (!fbval){
			printf("reached end of B\n");
			break;
		}
		
		if ( A == B){
			intersection++;
			faval = fscanf(inputA,"%x\n", &A);
		        fbval = fscanf(inputB,"%x\n", &B);
				
		} 
		else {
			if (A > B){
				sumB++;
                        	fbval = fscanf(inputB,"%x\n", &B);
			}
		     	else {
				sumA++;
				faval = fscanf(inputA,"%x\n", &A);
			} 
		}
	}
	if (!feof(inputA) && faval > 0 ){
		while(!feof(inputA)){
			sumA++;
                        faval = fscanf(inputA,"%x\n", &A);
		}
	}
	else if (!feof(inputB) && fbval > 0){
		while(!feof(inputB)){
                        sumB++;
                        fbval = fscanf(inputB,"%x\n", &B);
                }
	}
	fclose(inputA);
	fclose(inputB);
	float tanimoto = (float)intersection / (float)(sumA + sumB + intersection);	
	printf("tanimoto index = %5f\n",tanimoto);
	printf("unique A = %lu\n", sumA);
	printf("unique B = %lu\n", sumB);	
	return 0;
}
