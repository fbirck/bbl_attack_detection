#!/bin/bash



for fname in mibench_cfg/*cfg; do
	out=$(echo ${fname##*/}_temp)
	grep "\->" ${fname} | awk '{ print $1,$2}' > $out
	sed -i 's/\->//g' $out
	sed -i 's/\"//g' $out
	cat ${fname}_temp | tr 'a-z' 'A-Z0' | sort -u > ${out%%_*}_edgelist
	lc=$(wc -l ${out%%_*}_edgelist)
	echo "$fname has $lc edges" | tee -a edge_counts
	rm ${fname}_temp	
done;	
