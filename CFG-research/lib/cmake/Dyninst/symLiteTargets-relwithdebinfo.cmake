#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "symLite" for configuration "RelWithDebInfo"
set_property(TARGET symLite APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(symLite PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "/home/fbmoreira/lib/libsymLite.so.10.1.0"
  IMPORTED_SONAME_RELWITHDEBINFO "libsymLite.so.10.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS symLite )
list(APPEND _IMPORT_CHECK_FILES_FOR_symLite "/home/fbmoreira/lib/libsymLite.so.10.1.0" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
