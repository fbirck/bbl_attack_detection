#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "stackwalk" for configuration "RelWithDebInfo"
set_property(TARGET stackwalk APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(stackwalk PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "/home/fbmoreira/lib/libstackwalk.so.10.1.0"
  IMPORTED_SONAME_RELWITHDEBINFO "libstackwalk.so.10.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS stackwalk )
list(APPEND _IMPORT_CHECK_FILES_FOR_stackwalk "/home/fbmoreira/lib/libstackwalk.so.10.1.0" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
