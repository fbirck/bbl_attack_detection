# - Config file for Dyninst and its component libraries
# It defines the following variables:
#
# DYNINST_INCLUDE_DIRS
# DYNINST_LIBRARIES
# DYNINST_INTERNAL_DEFINES - used by the test suite

# compute paths

get_filename_component(DYNINST_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set (DYNINST_INCLUDE_DIR ${DYNINST_CMAKE_DIR}/../../../include)

# Library dependencies
#include ("${DYNINST_CMAKE_DIR}/DyninstTargets.cmake")
foreach(TARG ;common;dynElf;dynDwarf;symLite;instructionAPI;symtabAPI;parseAPI;pcontrol;stackwalk;patchAPI;dyninstAPI;dynC_API)
  include ("${DYNINST_CMAKE_DIR}/${TARG}Targets.cmake" OPTIONAL)
endforeach()

set (DYNINST_LIBRARIES "dyninstAPI")

# Other variables Dyninst mutators may depend on
set (DYNINST_PLATFORM "x86_64-unknown-linux2.4")
set (DYNINST_INTERNAL_DEFINES -Dcap_dynamic_heap;-Dcap_liveness;-Dcap_threads;-Dcap_gnu_demangler;-Dcap_32_64;-Dcap_fixpoint_gen;-Dcap_noaddr_gen;-Dcap_registers;-Dcap_stripped_binaries;-Dcap_tramp_liveness;-Dcap_stack_mods;-Dcap_async_events;-Dcap_binary_rewriter;-Dcap_dwarf;-Dcap_mutatee_traps;-Dcap_ptrace;-Dcap_thread_db;-Dbug_syscall_changepc_rewind;-Dbug_force_terminate_failure;-Darch_x86_64;-Darch_64bit;-Dos_linux;-Dx86_64_unknown_linux2_4)
set (USE_CXX11_ABI "")

if(DYNINST_FIND_COMPONENTS)
  foreach(COMP DYNINST_FIND_COMPONENTS)
    if(NOT TARGET ${COMP})
      set(DYNINST_${COMP}_FOUND 0)
      if(DYNINST_FIND_REQUIRED_${COMP})
	MESSAGE(ERROR "${COMP} was not part of the Dyninst build")
      endif()
    else()
      set(DYNINST_${COMP}_FOUND 1)
      MESSAGE(STATUS "Found ${COMP}")
    endif()
  endforeach()
endif()

if(TARGET dyninstAPI)
  set(Dyninst_FOUND 1)
else()
  set(Dyninst_FOUND 0)
endif()

