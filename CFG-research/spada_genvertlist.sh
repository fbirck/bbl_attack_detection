#!/bin/bash



for bench_name in mibench_bins/*; do	
	for fname in CBBT-process/${bench_name##*/}_traces/maintrace*; do 
		outf=$(echo ${bench_name##*/}_${fname##*/}_spadavertlist)
		sort -g -k 1 -u $fname | cut -d ' ' -f 1 > saida
		cat saida | xargs printf "%4s\n" | tr 'a-z' 'A-Z0' | sort -u > $outf
		lc=$(wc -l $outf)
		echo $lc | tee -a spada_fcfg_counts	
		rm saida
	done;
done;	
