#!/bin/bash



#first execute cfg_gen, might need to set LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/fbmoreira/bbl_attack_detection/CFG-research/lib/
./cfg_genfunction $1 > appgraph

#we got the appgraph. now we extract function strings along addresses in binary
grep "label =" appgraph | awk -F = '{print $1,$2}' | tr -d []\; | tr -d \" | awk -F '\' '{print $1}' | awk '{print $1,$3}' > ${2}_labels

#and now we must extract the valid function transitions. basically, calls appear alongside a color=blue and rets appear alongside a color=green in the appgraph, so it *shouldn't* be hard
grep "blue" appgraph | grep "\->" | tr -d \" | awk '{print $1,$3}' | sort | uniq > ${2}_calls
grep "green" appgraph | grep "\->" | tr -d \" | awk '{print $1,$3}' | sort | uniq > ${2}_rets

