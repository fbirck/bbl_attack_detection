import numpy as np
from scipy.sparse import *
import math
from sklearn.decomposition import PCA
from sklearn import svm
import sys
import glob
import os.path
import heapq
import pickle
from sklearn.model_selection import RepeatedKFold
import pymp

def createprofiles(cfgname,traces):
    cfgdict = {} #CFGdict contains all possible transitions

    labelname = cfgname + "_labels"
    #open labels
    labelfile = open(labelname, 'r')
    symbolset = set()
    addr_func_dict = {}
    for lines in labelfile:
        fields = lines.split()
        symbolset.add(fields[1])
        addr_func_dict[fields[0]] = fields[1]

    addr_func_dict['ffffffffffffffff'] = 'NIL' #special character for special targets in dynlib or something else that libparseAPI detects.
    labelfile.close()

    #DONE FOR LABELS. TESTED.

    callname = cfgname + "_calls"
    callfile = open(callname, 'r')
    for lines in callfile:
        fields = lines.split()
        if fields[0] in addr_func_dict:
            funcname1 = addr_func_dict[fields[0]]
        else:
            print("inexistent target addr")

        if fields[1] in addr_func_dict:
            funcname2 = addr_func_dict[fields[1]]
        cfgdict[funcname1,funcname2] = 1

    callfile.close()

    #DONE FOR CALLS
    print("finished extracting application CFG")
    ##################### ret is not needed according to Dr. Shu
    #code is just a copy, if needed

    #finished static cfg
    ##################################################

    freqdictlist = []
    path = traces + '*' + '.func'
    #read every trace and train
    itt = 1
    for tracename in glob.glob(path):

        tracefreqdict = {}
        #open CFG trace file
        tracefile = open(tracename, 'r')

        print("began processing trace " + str(tracename) + " which is " + str(itt))
        itt = itt + 1
        #create a symbol dict addr -> funcname
        #for each line in file:
        numcalls = 0
        callstack = []
        for lines in tracefile:
            #split line
            fields = lines.split()
            if fields[0] == '#' or fields[0] == '@': #invalid line
                continue
            if fields[0] == 'Return': #if returner
                #add symbols
                if len(fields) > 2:#we only grab things which we can track a discernible img/bin/offset combination.
                    if len(callstack) > 0: #could be possibly empty due to exceptions, longjmp, etc... dunno! gotta analyze better.
                        rettgt_id = callstack.pop()
#                    print("callstack after ret")
#                    print(callstack)
                    ret_id = fields[2]
                    func = ret_id.split(":")[1]
                    symb = func.split("+")[0]
                    symbolset.add(symb)
            else:
                if len(fields) > 5: #we only grab things which we can track a discernible img/bin/offset combination.
                    caller_id = fields[2]
                    callee_id = fields[5]
                    if fields[0] == 'Call':
                        #push caller id into callstack
                        numcalls = numcalls + 1
                        callstack.append(caller_id)

                    func = caller_id.split(":")[1]
                    symbcaller = func.split("+")[0]
                    symbcaller = symbcaller.split("@")[0]
                    #enter caller/returner in dict, without addr, since Shu does not care about it.
                    symbolset.add(symbcaller)
                    
                    func = callee_id.split(":")[1]
                    symbcallee = func.split("+")[0]
                    symbcallee = symbcallee.split("@")[0]
                    #enter calle in dict, without addr
                    symbolset.add(symbcallee)
                    #add transition
                    cfgdict[symbcaller, symbcallee] = 1
                    if (symbcaller,symbcallee) in tracefreqdict:
                        tracefreqdict[symbcaller,symbcallee] = tracefreqdict[symbcaller,symbcallee] + 1
                    else:
                        tracefreqdict[symbcaller,symbcallee] = 1
        tracefile.close()
        print("finished processing trace " + str(tracename) + " file\n")
        #for k,v in tracefreqdict.items():
            #tracefreqdict[k] = v / numcalls  ###should we divide by numcalls or not?
        freqdictlist.append(tracefreqdict)

    #caught all symbols, caught all freqs. still divided

    #now we have all symbols in the symbolset for all training traces, let's convert the intermediary frequency matrices into O and F.
    #first, create a symbol->index mapping
    index_to_symbols = np.array(list(symbolset))

    profiles = []
    #second, create the matrix for each trace.
    #initialize
    print("constrcting behaviors set with symbolset size = " + str(len(symbolset)))
    setsize = len(symbolset)
    
    entryit=1
    for entries in freqdictlist:
        FMAT = dok_matrix((setsize, setsize), dtype=np.float64)
        OMAT = dok_matrix((setsize, setsize), dtype=bool)
        
        #for i in range(0,setsize):
        #    for j in range(0,setsize): #or exist, as checking in dictionary
        #            FMAT[i,j] = 0
        for k,v in entries.items():
            idxs1 = np.where(index_to_symbols == k[0])[0][0]
            idxs2 = np.where(index_to_symbols == k[1])[0][0]
            FMAT[idxs1,idxs2] = v 
            OMAT[idxs1,idxs2] = True
        profiles.append((OMAT,FMAT))
        entryit = entryit + 1
        
    print("finished constructing behaviors set")
    return profiles, index_to_symbols
    #produced bs: Os and Fs, also returns dimension.
#####################################################################################################
#####################################################################################################
#####################################################################################################
#####################################################################################################
#############


def impl_OR(O1, O2,dim): #receiving dict in tuple form
    O1dok = dok_matrix((dim,dim), dtype=np.bool)
    O1dok._update(dict(O1)) #recreate dok
    npO1 = np.array(dok_matrix.asformat(O1dok, "array"))

    O2dok = dok_matrix((dim,dim), dtype=np.bool)
    O2dok._update(dict(O2)) #recreate dok
    npO2 = np.array(dok_matrix.asformat(O2dok, "array"))

    npO3 = np.logical_or(npO1, npO2)
    O3dok = dok_matrix(npO3)
    #for i in range(0,np.shape(O1dok)[0]):
    #    for j in range(0,np.shape(O1dok)[1]):
    #       O3dok[i,j] = O1dok[i,j] or O2dok[i,j]
    return tuple(O3dok.items())

def impl_AND(O1, O2, dim): #receiving dict in tuple form
    O1dok = dok_matrix((dim,dim), dtype=np.bool)
    O1dok._update(dict(O1)) #recreate dok
    npO1 = np.array(dok_matrix.asformat(O1dok, "array"))

    O2dok = dok_matrix((dim,dim), dtype=np.bool)
    O2dok._update(dict(O2)) #recreate dok
    npO2 = np.array(dok_matrix.asformat(O2dok, "array"))

    npO3 = np.logical_and(npO1, npO2)
    O3dok = dok_matrix(npO3)
    
    #for i in range(0,np.shape(O1dok)[0]):
    #    for j in range(0,np.shape(O1dok)[1]):
    #       O3dok[i,j] = O1dok[i,j] and O2dok[i,j]
    return tuple(O3dok.items())

def impl_DIST(O1, O2, dim): #receiving dict in tuple form

    O1dok = dok_matrix((dim,dim), dtype=np.bool)
    O1dok._update(dict(O1)) #recreate dok
    
    O2dok = dok_matrix((dim,dim), dtype=np.bool)
    O2dok._update(dict(O2)) #recreate dok
    
    O3dok = O1dok
    ham = 0
    O1t = np.sum(O1dok)
    O2t = np.sum(O2dok)
    ham = np.sum(O1dok != O2dok) 
    #for i in range(0,np.shape(O1dok)[0]):
    #            for j in range(0,np.shape(O1dok)[1]):
                   # if O1dok[i,j] == True:
                   #     O1t = O1t + 1
                   # if O2dok[i,j] == True:
                   #     O2t = O2t + 1
                  #  if O1dok[i,j] == O2dok[i,j]:
                   #     ham  = ham + 1
    if O1t > O2t:
        O1t = O2t 
    return ham / O1t

def impl_PEN(int1, int2):
    l1 = math.log(int1)
    l2 = math.log(int2)
    if l1 > l2:
        return l1
    else:
        return l2

def behaviorclustering(profiles, dim, dist_threshold):
    myheap = [] #empty list to represent our heap
    vhash = {}
    Vset = set()

    print("begin behavior clustering")
    itb = 1
    for behavior in profiles:
        #print("doing behavior " + str(itb))
        itb = itb + 1
        O = tuple(behavior[0].items()) #transform to tuple asap, then to dok

        if O in vhash:
            vhash[O] = vhash[O] + 1 #as tuple, can't do dict
        else:
            vhash[O] = 1
        for OI in Vset:
            distance = impl_DIST(O, OI, dim) * impl_PEN(vhash[O], vhash[OI]) #must pass both as tuple, transform in function
            heapq.heappush(myheap,(distance, O, vhash[O], OI, vhash[OI]))   #as tuple, to ensure no problems afterward
        Vset.add(O) #as tuple, can't do dict

#    print("check4: Vset size is not empty, it is " + str(len(Vset)))
#    print("check5: heap is not empty, it is ")
    heapq.heapify(myheap)
#    print("got heap len " + str(len(myheap)))
#    print("got the following dists in heap")
    print("finished constructing heap of length " + str(len(myheap)))
    while (len(myheap) != 0):
        aux = heapq.heappop(myheap)
#        print("check6: popped dist of " + str(aux[0]))
        dp = aux[0]
        O1 = aux[1]
        vo1 = aux[2]
        O2 = aux[3]
        vo2 = aux[4]
        if dp > dist_threshold:
            break
        else:
            if (O1 in Vset) and (O2 in Vset):
                if (vo1 < vhash[O1]) and (vo2 < vhash[O2]):
                    continue
                O = impl_OR(O1, O2,dim)
                vhash[O] = vhash[O1] + vhash[O2]
                print("about to remove these Os")
                print(O1)
                print("and")
                print(O2)
                Vset.remove(O1)
                if (O1 != O2):
                    Vset.remove(O2)

                for OI in Vset:
                    distance = impl_DIST(O,OI, dim) * impl_PEN(vhash[O], vhash[OI])
                    heapq.heappush(myheap, (distance, O, vhash[O], OI, vhash[OI]))
                Vset.add(O) #as tuple, not dict

#    print("check7: Vset is not empty, got : ")
#    print(len(Vset))
#    print("exited with heap size = " + str(len(myheap)))
    whash = {}
    print("finished processing heap")
    for O in Vset:
        whash[O] = set()
    for behavior in profiles:
#        print("check 8 entered in search of a cluster for behavior O")
        O = tuple(behavior[0].items())
        m = sys.maxsize
        VIset = set()
        for OI in Vset:
#            print("     check9: entered here to search for a behavior comparable to O")
            if impl_OR(O,OI,dim) == OI:
#                print("         check10: found O equal to OI")
                osdist = impl_DIST(O, OI, dim)
                if osdist < m:
#                    print("             check11 found osdist smaller than m, it is " + str(osdist))
                    m = osdist
                    VIset.clear()
                    VIset.add(OI)
                else:
#                    print("             check12 found osdist not smaller than m, it is " + str(osdist))
                    if osdist == m:
#                        print("                 check13 added something to VIset")
                        VIset.add(OI)
           # else:
                #print("exceptional diff in Os")
        #print("check14 went to add behaviors in VIset of len " + str(len(VIset)))
        for Oidx in VIset: 
            whash[Oidx].add((tuple(behavior[0].items()), tuple(behavior[1].items())))

    Clusters = set()
    for O in Vset:
 #       print("check15 adding whash set of size " + str(len(whash[O])) + " to clusters")
        Clusters.add((O,frozenset(whash[O])))
    return Clusters
#got clusters
################################
################################
################################
################################

def impl_OC5(cluster, dim):
    for x in cluster[1]:
        break
    print("cluster is")
    print(cluster)
    if 'x' in locals(): 
        stacker = x[0]
        for behaviors in cluster[1]:
            stacker = impl_AND(stacker,behaviors[0],dim)
        return stacker
    return 0

####STEP 8:
def cooc_detection(b, Clusterinfo,dim): #behavior, clusters
    fittingclusters = []
    O = tuple(b[0].items())
    for boundedcluster in Clusterinfo:
        cluster = boundedcluster[0]
        vsvm = boundedcluster[1]
        svmdist = boundedcluster[2]
        clusterO = impl_OC5(cluster, dim)
        ored = impl_OR(O,cluster[0],dim)
        same1 = (dict(ored) == dict(cluster[0])) 
        if (same1):
            anded = impl_AND(O, clusterO, dim)
            same2 = (dict(anded) == dict(clusterO))
            k1 = len(dict(anded).keys())
            k2 = len(dict(clusterO).keys())
            k3 = len(dict(ored).keys())
            if (same2):
                print("check2, added a cluster")
                cd =  impl_DIST(b[0], cluster[0], dim)
                fittingclusters.append((cd,boundedcluster))

    if len(fittingclusters) == 0:
        print("anomalous behavior! no co-oc cluster found!")
        return None
    fittingclusters.sort()
    selectedclusters = set()
    smallestdist = fittingclusters[0][0]
    for fitclr in fittingclusters:
        if fitclr[0] > smallestdist:
            break
        selectedclusters.add(fitclr[1])
    return selectedclusters


def impl_APPLOG(F):
    for i in range(0,np.shape(F)[0]):
        for j in range(0,np.shape(F)[1]):
           F[i,j] = math.log(F[i,j] + 1, 2)
    return F

def intracluster_modeling(C,dim):
    SVMF = pymp.shared.list()
    i = 0
    LC = list(C)
    print("got " + str(len(LC)) + " agg clusters")
    ##
    with pymp.Parallel(32) as p:
        for index in p.range(0,len(LC)):   
            clusters = LC[index]
     ##for clusters in C
            inputarray = np.array([])
            print("processing cluster " + str(index))
            if len(clusters[1]) > 10:
                for b in clusters[1]:       # for each cluster, define SVM
                    dokmatF = dok_matrix((dim,dim), dtype=np.float64)
                    dokmatF._update(dict(b[1]))
                    newF = impl_APPLOG(dokmatF) #b's F will be changed
                    inputarray = np.append(inputarray, newF.todense().flatten()) #flatten the mat into a np array
                    print("appended " + str(np.size(newF.todense().flatten())) + " elements to array for cluster" + str(index))
                    #transform to numpy
            else:
                print("cluster " + str(index) + " only has " + str(len(clusters[1])) + " behaviors")
            with p.lock:
                SVMF.append((clusters,None,0))
                continue
            
            inputarray = np.reshape(inputarray, (len(clusters[1]), -1))
            #perform PCA if we got more than 10 samples
            print("To process: input array shape for cluster " + str(index) + " is ")
            print(np.shape(inputarray))
            pcamodel = PCA(n_components=0.95)
            pcamodel.fit(inputarray)
            
            #perform k-folded SVM
            FPR_UPPER = 0.0001
            svminput =  pcamodel.transform(inputarray)
            kf = RepeatedKFold(n_splits=10,n_repeats = 10, random_state=42)
            
            allowed_dist = 0
            k = int((np.shape(svminput)[0])/2)  # initialize a restrictive k given input size... k is not well-defined by Shu et al. previously had k = len(svminput) * FPR_UPPER, but that can't be good.
            #what is a 'restrictive' value here? larger or shorter?
            print("initial k is " + str(k) + " for cluster " + str(index))
            vsvm = svm.OneClassSVM(kernel="rbf",gamma='scale')
            TF = 0
            for train_index, test_index in kf.split(svminput):
                    vsvm.fit(svminput[train_index])
                    alldist = vsvm.decision_function(svminput[test_index])
                    sorteddist = np.sort(alldist)
                    TF = sorteddist[int(k)]
                    negs = np.count_nonzero( sorteddist < TF)
                    FPR_obt = negs/np.size(alldist)
                    if FPR_obt > FPR_UPPER:
                        k = int(k - 1)        #decrease k to get less restrictive 
                        print("decreasing k to " + str(k) + ", since TF was " + str(TF), + " and FPR was " + str(FPR_obt))
                    if (k < 0): #could we ever come to this?
                        k = 0;

            print("will add vsvm to list")
            with p.lock:
                SVMF.append((clusters,vsvm,TF))
    return SVMF

def singlecluster_deterministic_method(Cluster):#calculate variance of events and determine frequency range for 0-variance events
    matlist = []
    Cmats = set()
    for behaviors in Cluster[1]:
        matlist.append(behaviors[1])
    mat = np.asarray(matlist)
#    mat = np.reshape(mat, (len(Cluster[1]),-1))
    #calculate variance of events. 
    vararr = np.var(mat,axis=0)
    fmin = np.amin(mat,axis=0)
    fmax = np.amax(mat,axis=0)
    return (vararr,fmin,fmax) #returns numpy matrices with variance, minimum frequency and maximum frequency

############

#STEP 9: 3-rule detection method

def singlecluster_occurrence_freq_analysis(b,boundedcluster): #the description in the paper must be wrong, or it is nonsensical

    cluster = boundedcluster[0]
    vsvm = boundedcluster[1]
    svmdist = boundedcluster[2]
    
    #Rule3: normal if behavior fits in presumption of innocence in tiny clusters
    if ((len(cluster[1]) < 30) or (vsvm == None)): #what is a tiny cluster...?
        print("returned normal due to tiny cluster")
        return True
    else:
    
        #Rule1: normal if behavior instance b passes probabilistic method
        rule1 = False
        highb = b[1].todense().flatten() #vectorize
        distb = vsvm.decision_function(highb)
        if distb < svmdist:
            rule1 = False
        else:
            rule1 = True
        
        rule2 = True
        #Rule2: normal if behavior instance b passes deterministic method
        detmats = singlecluster_deterministic_method(cluster)
        for i in range(0,detmats[0].size):
            if detmats[i] < 0.1:
                if highb[i] < detmats[1][i]:
                    rule2 = False
                if highb[i] > detmats[2][i]:
                    rule2 = False
        
        if (rule1 and rule2):
            return True
        else:
            return False


def cluster_occ_freq_analysis(b,C):
    for clusters in C:
        normal = singlecluster_occurrence_freq_analysis(b,clusters)
        if normal:
            return True
    return False


def detect_normalness(b,C,dim):
    #first, run co-oc
    fitclusters = cooc_detection(b,C,dim)
    if fitclusters == None:
        return False #does not pass co-oc
    #print("got the following fit clusters")
    #for c in fitclusters:
    #    print(c)
    #then run cluster_occ_freq with info from co-oc
    normal = cluster_occ_freq_analysis(b,fitclusters)    
    return normal

###########################3#
def translate_profile(mapnp, tracename):
    #reads a profile and creates a behavior
    tracefile = open(tracename, 'r')
    tracefreqdict = {} #creating a new tracefreqdict that represents the input
    #create a symbol dict addr -> funcname
    #for each line in file:
    numcalls = 0
    callstack = []
    for lines in tracefile:
        #split line
        fields = lines.split()
        if fields[0] == '#' or fields[0] == '@': #invalid line
            continue
        if fields[0] == 'Return': #if returner
            #add symbols
            if len(fields) > 2:#we only grab things which we can track a discernible img/bin/offset combination.
                if len(callstack) > 0: #could be possibly empty due to... dunno! gotta analyze better.
                    rettgt_id = callstack.pop()
        else:
            if len(fields) > 5: #we only grab things which we can track a discernible img/bin/offset combination.
                caller_id = fields[2]
                callee_id = fields[5]
                if fields[0] == 'Call':
                    #push caller id into callstack
                    numcalls = numcalls + 1
                    callstack.append(caller_id)

                func = caller_id.split(":")[1]
                symbcaller = func.split("+")[0]
                symbcaller = symbcaller.split("@")[0]
                
                func = callee_id.split(":")[1]
                symbcallee = func.split("+")[0]
                symbcallee = symbcallee.split("@")[0]
                #add transition
                if (symbcaller,symbcallee) in tracefreqdict:
                    tracefreqdict[symbcaller,symbcallee] = tracefreqdict[symbcaller,symbcallee] + 1
                else:
                    tracefreqdict[symbcaller,symbcallee] = 1
    tracefile.close()
    setsize = len(mapnp)
    FMAT = dok_matrix((setsize, setsize), dtype=np.float64)
    OMAT = dok_matrix((setsize, setsize), dtype=bool)
    for i in range(0,setsize):
        for j in range(0,setsize): #or exist, as checking in dictionary
                FMAT[i,j] = 0
    for k,v in tracefreqdict.items():
        if ((k[0] not in mapnp) or (k[1] not in mapnp)):
            print ("anomaly detected, unrecognized label (either " + str(k[0]) + " or " + str(k[1]) + " was not found in the trained symbolset")
            exit(1)

        idxs1 = np.where(mapnp == k[0])[0][0]
        idxs2 = np.where(mapnp == k[1])[0][0]
        FMAT[idxs1,idxs2] = v 
        OMAT[idxs1,idxs2] = True
    #for k,v in tracefreqdict.items():
        #tracefreqdict[k] = v / numcalls  ###should we divide by numcalls or not?
    return (OMAT,FMAT)


######################################################################

def clusterprofiles(staticfile, traceglobf, maxdist, outputname):
    profiles, symbolmap = createprofiles(staticfile, traceglobf)
    dim = symbolmap.size
    clusters = behaviorclustering(profiles, dim, float(maxdist))

    sfile = open(outputname, 'wb')
    pickle.dump((symbolmap,clusters),sfile)
    sfile.close()
####################################################
def train_intracluster(inputmodel,outputmodel):
    mfile = open(inputmodel, 'rb')
    tp = pickle.load(mfile)
    symbolmap = tp[0]
    dim = symbolmap.size
    clusters = tp[1]
    mfile.close()
    
    lala = intracluster_modeling(clusters,dim)
    print("result of intracluster modeling")
    print(lala) 

    sfile = open(outputmodel, 'wb')
    pickle.dump((symbolmap,list(lala)),sfile)
    sfile.close()

###############################################
def trydetection(inputmodel, inputtrace):
    mfile = open(inputmodel, 'rb')
    tp = pickle.load(mfile)
    symbolmap = tp[0]
    bounded_clusters = tp[1]
    mfile.close()
    print("got " + str(len(bounded_clusters)) + " clusters in the loaded model")
    print("cluster sizes are : ")
    for i in bounded_clusters:
        print(len(i[0][1]))
    dim = symbolmap.size
    t = translate_profile(symbolmap, inputtrace)
    print("for input trace " + str(inputtrace) + " we got ")
    normal = detect_normalness(t,bounded_clusters,dim) 
    print("normal = " + str(normal))
    return normal
###############################################################################
def read_config(configfile):
    confreader = open(configfile, 'r')
    for lines in confreader:
        fields = lines.split()
        print(fields)
        return fields
        #if MODE == 0:
        #    arglist.append(lines[1])
        #    arglist.append(lines[2]
        #    CParg3 = lines[3]
        #    CParg4 = lines[4]
        #elif MODE == 1:
        #    TIarg1 = lines[1]
        #    TIarg2 = lines[2]
        #elif MODE == 2:
        #    DETarg1 = lines[1]
        #    DETarg2 = lines[2]
    return arglist
###############################################################################
def main():
    
    arglist = read_config(sys.argv[1])
    MODE = int(arglist[0])
    if (MODE == 0):
        #clusterprofiles(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
        clusterprofiles(arglist[1], arglist[2], arglist[3], arglist[4])
    elif (MODE == 1):
        #train_intracluster(sys.argv[1], sys.argv[2])
        train_intracluster(arglist[1], arglist[2] )
    elif (MODE == 2):
        #trydetection(sys.argv[1], sys.argv[2])
        trydetection(arglist[1],arglist[2])

if __name__ == "__main__":
    main()
