#!/bin/bash


#echo "expects static files path,trainfolder, testfolder, attack output file, and normal output file"
#statfile=${1}
#trainfolder=${2}
#testfolder=${3}
#atkout=${7}
#normout=${5}


source $1

if [ -z "$trainfolder" ]; then
	echo "invalid trainfolder"
	exit
fi;

if [ -z "$testfolder" ]; then
	echo "invalid testfolder"
	exit
fi;

if [ -z "${statfile}_labels" ]; then
	echo "invalid statfile"
	exit
fi;

#HARDCODED DIST
dist=2

echo "training first model with training folder: $trainfolder"
echo "0 $statfile $trainfolder $dist 7PROCMAILMODEL1" > auxfile 
python3 proctraces.py auxfile
###############################
echo "1 7PROCMAILMODEL1 7PROCMAILMODEL2" > auxfile
echo "training second model"
python3 proctraces.py auxfile
###############################

echo "testing atks"
for fname in ${testfolder}/atk_trace_*bbl; do
	echo "processing $fname"
	echo "2 7PROCMAILMODEL2 $fname" > auxfile 
	python3 proctraces.py auxfile >> $2
done;
echo "testing normals"
for fname in ${testfolder}/test_procmail_*func; do
	echo "processing $fname"
	echo "2 7PROCMAILMODEL2 $fname" > auxfile 
	python3 proctraces.py auxfile >> $3
done;
