#!/bin/bash

#input format is: ./execute_test.sh test.cfg
#test.cfg is a file defining the following variables:

#1)trainfolder: folder where traces used for training are found

#2)traintraceprefix: prefix of files used for training.

#3)testfolder: folder where traces used for testing are found

#4)testnormprefix: prefix of trace files with normal behavior

#5)testatkprefix: prefix of trace files with anomalous behavior

#optional:
#6)binary: path to binary so the script can obtain .text size and offset

#7)p: granularity parameter to filter cbbt-phases by size.
#no filters will be applied to cbbts when omitting "binary" and "p"


#IMPORTANT NOTE: all files must be sequentially labeled i.e. trace_1.bbl, trace_2.bbl, etc.

cfgfile=$1
source ${cfgfile}

if [ -z "$trainfolder" ]; then
	echo "invalid trainfolder"
	exit
fi;

if [ -z "$testfolder" ]; then
	echo "invalid testfolder"
	exit
fi;


#======================================

#------------------ START CBBT-process

#create cbbt files. Assumes files from BBL-sim, which are bbl traces (indicated by .bbl suffix)
cd CBBT-process/ ;
./scr_batch_cbbtgenerate.sh "${trainfolder}/*.bbl"

echo "generated cbbts for training inputs in ${trainfolder}"

#filter cbbts. Creates single .filteredcbbts file representing all cbbts of the benchmark
if [ -z "$binary" ]; then
	./scr_batch_cbbtprocess.sh "nofilter" "${trainfolder}/"
	echo "created filtered cbbts for training inputs with NO FILTERS"
else
	./scr_batch_cbbtprocess.sh "filter" "${trainfolder}/" $binary $p
	echo "created filtered cbbts for training inputs using binary ${binary} and granularity ${p}" 
fi;

#fix for general script
mv ${trainfolder}/.filteredcbbts ${trainfolder}/filteredcbbts

#clean mess made by cbbtgenerate
rm ${trainfolder}/*.cbbts 

cd ../

echo "finished CBBT process"
echo " "
echo " "
#------------------ END CBBT-process

#======================================

#------------------ START feature-training
cd feature-training/ ;

echo "creating phase features for training inputs' cbbts"
#create cbbt-phase features in separate .pfs files, return all concatenated files in a single "behavior" file
./scr_gentrain.sh ${trainfolder}/filteredcbbts ${trainfolder}/behavior "${trainfolder}/${traintraceprefix}" ".bbl" 

echo "creating phase features for test inputs"
./scr_genpfs.sh ${trainfolder}/filteredcbbts "${testfolder}/${testnormprefix}" ".bbl"
./scr_genpfs.sh ${trainfolder}/filteredcbbts "${testfolder}/${testatkprefix}" ".bbl"

cd ../

echo "finished feature-training"
echo " "
echo " "

#------------------ END feature-training

#======================================

#------------------ START STAT-testing
cd STAT-testing/ ;

#first, let's catch the maximum and minimum index of testfolder traces
tmaxindex=$(ls ${testfolder}/ | grep -oh -E "${testnormprefix}[0-9]+.bbl" |  awk -v var=${testnormprefix} '{gsub(var,""); print}' | sort -t . -k 1 -n | tail -n 1 | awk '{gsub(".bbl",""); print}')
tminindex=$(ls ${testfolder}/ | grep -oh -E "${testnormprefix}[0-9]+.bbl" |  awk -v var=${testnormprefix} '{gsub(var,""); print}' | sort -t . -k 1 -n | head -n 1 | awk '{gsub(".bbl",""); print}')

amaxindex=$(ls ${testfolder}/ | grep -oh -E "${testatkprefix}[0-9]+.bbl" |  awk -v var=${testatkprefix} '{gsub(var,""); print}' | sort -t . -k 1 -n | tail -n 1 | awk '{gsub(".bbl",""); print}')
aminindex=$(ls ${testfolder}/ | grep -oh -E "${testatkprefix}[0-9]+.bbl" |  awk -v var=${testatkprefix} '{gsub(var,""); print}' | sort -t . -k 1 -n | head -n 1 | awk '{gsub(".bbl",""); print}')

behaviorfile=${trainfolder}/behavior

#first, let's create the different required files: #careful rangemax accumulates
./scr_makemax.sh ${behaviorfile} ${trainfolder}/rangemax_uniqbbl

echo "checking normals in testfolder ${testfolder}"
./fastcheckermax ${trainfolder}/rangemax_uniqbbl "${testfolder}/${testnormprefix}" ".pfs" ${tminindex} ${tmaxindex} 
./fastcheckermaxphases ${behaviorfile} ${trainfolder}/rangemax_uniqbbl "${testfolder}/${testnormprefix}" ".pfs" ${tminindex} ${tmaxindex}

echo "checking anomalies in testfolder ${testfolder}"
./fastcheckermax ${trainfolder}/rangemax_uniqbbl "${testfolder}/${testatkprefix}" ".bbl" ${aminindex} ${amaxindex} 
./fastcheckermaxphases ${behaviorfile} ${trainfolder}/rangemax_uniqbbl "${testfolder}/${testatkprefix}" ".pfs" ${aminindex} ${amaxindex}

