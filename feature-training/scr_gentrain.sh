#!/bin/bash

#this script expects a file with processed cbbts (output of cbbtprocess.py), an output file name, and a prefix and suffix for traces to be partitioned and characterized by the cbbt.
#It expects numbers to index different traces in the format ${prefix}+([0-9])${suffix}, so be careful when naming your traces.

#name of file to be used as cbbts (output of cbbtprocess.py)
fname=$1


echo "filtered cbbts file is $fname"

#name of output file
outname=$2
echo "output file name is $outname"

#prefix of application trace file names
prefix=$3
echo "traces prefix is $prefix"

#suffix of application trace file names
suffix=$4
echo "traces suffix is $suffix"

status=$(shopt extglob | awk '{print $2}')

shopt -s extglob #activate extended glob pathing

#generate cbbt traces for all bbl traces.
for trace_name in ${prefix}+([0-9])${suffix}; do
	./bbl_simulate $fname $trace_name ${trace_name}.partialbehavior & 
done;
wait

if [[ $status -ne "on" ]]; then
	shopt -u extglob;
fi 

cat ${prefix}*.partialbehavior >> $outname
rm ${prefix}*.partialbehavior
