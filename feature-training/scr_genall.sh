#!/bin/bash

#for all mibench apps, create cbbt traces of all inputs, and create a single file ${app}_cbbt (3rd parameter of scr_gentrain) which will contain all traces concatenated. 
# This is not really necessary unless you want to make a behavior based on all traces (i.e. you split your traces into two folders, one with training inputs and the other with the samples.



for fname in ../mibench_bins/*;do
	app=${fname##*/}
	input_name=$(echo "${app}_selected_nofilter/${app}_all")
	./scr_gentrain.sh $input_name ${app}_cbbt_nofilter $app ".bbl" & 
done;
wait
