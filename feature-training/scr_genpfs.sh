#!/bin/bash

#name of file to be used as cbbts 
fname=$1

echo "fname is $fname"

#name of prefix
prefix=$2
echo "prefix is $prefix"

suffix=$3
echo "suffix is $suffix"

status=$(shopt extglob | awk '{print $2}')

shopt -s extglob #activate extended glob pathing

#generate cbbt traces for bbltraces 1 to i.
for trace_name in ${prefix}+([0-9])${suffix}; do
	echo "calling bbl_simulate with $fname $trace_name ${trace_name%.*}.pfs"
	./bbl_simulate $fname $trace_name ${trace_name%.*}.pfs & 
done;
wait

if [[ $status -ne "on" ]]; then
	shopt -u extglob;
fi
