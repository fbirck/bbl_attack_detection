#!/bin/bash

#tool preparation

set -o errexit -o nounset -o pipefail -o errtrace -o posix

# directory of this script
DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# recompile pintool if necessary
(cd $DIR; make -q || make)

#------------------------------
#tool execution
mypath=$(echo "$HOME/bbl_attack_detection")
#it's VERY important to specify a full path or some spawned processes under other users and folders can get errno when trying to generate traces
#change this to wherever you installed pin 2.14. We provide this pin in the repo with the following path:
#../pin/pin.sh -ifeellucky -injection child -follow_execv -t obj-*/bbl_trace.so -o maintrace -c childerino -- $@
${mypath}/pin/pin -follow_execv -injection child -t ${mypath}/BBL-sim/obj-intel64/bbl_trace.so -o ${mypath}/BBL-sim/trace -m 0 -logfile ${mypath}/BBL-sim/logfile -- $@

