#bbl_tracer v1.0

This folder contains a Pin tool designed to generate an application's trace of basic blocks and their number of instructions. 
It was created and runs using **Pin-3.7**.
These information are used by other tools to derive critical basic block transitions, and therefore program phases.

The 3 main files are:
1. runbbltrace.sh
2. makefile
3. bbl_trace.cpp

- runbbltrace.sh

The tool can be used by invoking the script **runbbltrace.sh**.

Usage of the tool: ./runbbltrace.sh "application input"
Example(1): `./runbbltrace.sh ls` generates the bbl trace of the linux "ls" application
Example(2): `./runbbltrace.sh gcc -O3 input.c -o programtest` generates the bbltrace of gcc running with O3 optimization flag given input input.c and output programtest.

The traces are generated in the "traceprefix_'app'_pid" file, where "traceprefix" can be defined in the runbbltrace.sh execution command using the -o flag in the pin tool, 'app' is obtained by observing the /proc/pid/status first line, and pid is the process id.
Suffixes will always be the PID of the process being traced.

________

- makefile
Makefile which contains configurations and definitions for the compilation of the tool.
Please notice that we have pin-3.7 in our implementation, in the bbl_attack_detection/pin path.

__________

- bbl_trace.cpp

The file bbl_trace.cpp contains the tool code.
The tool supports multi-threaded applications and some forms of child processes.
It has 2 execution modes: fast and safe(default).
The mode can be selected with the *-m* flag.

	- Fast mode:
	Fast mode opens a single file handler per pin executable and only closes the file when the program ends.
	It will handle regular children and forks without any issue, and it is the recommended way to trace applications.

	- Safe mode:
	Safe mode only opens a file handler in inserted calls, writes to the file, and closes it immediately.
	Since Pin does not register certain system calls as forks/children, apparently it cannot follow some subshells in some programs.
	To this end, we designed safe mode to run these exceptional cases, such as procmail when replicating the Shellshock exploit.

______________

Restrictions:

There are some restrictions regarding scenarios where the code works.
For instance, depending on the form of process spawning used, we cannot detect some nginx processes. 
The other thread bbls will be mixed with the main thread trace, making a mess.
Furthermore, for reproducibility, ASLR **MUST** be shutdown, otherwise dynamic libraries might be relocated to different positions
in different executions. To do so, run the command:

echo 0 | sudo tee /proc/sys/kernel/randomize_va_space

If you are on Windows, you might want to take a look at the Enhanced Mitigation Experience Toolkit,
where you can selectively disable ASLR for a process: https://technet.microsoft.com/en-us/security/jj653751

Additionally, there is a restriction on how Pin defines basic blocks.
Due to some instructions (such as jump register) having unpredictable targets, an initial basic block might be split into two basic blocks
when a branch jumps to the middle of the initial basic block. Pin will then update its TRACE and BBL markers, although some small inaccuracies might happen
due to this. Upon tests, we found this to be negligible: we simply ended up with extra critical transitions which happened once.

Example:

First time Pin sees B1, B1 goes to B2. We use the first address of B1, B1Add, to mark B1, and the first address of B2, B2Add, to mark B2.
Once a jump register jumps to the middle of B1, B1 is split into B1.1 and B1.2.
B1Add marks the first address of B1.1, B1.2Add marks the first address of B1.2.
Now, wherever we pass through the sequence of B1Add->B2Add, we will actually see B1Add -> B1.2Add -> B2Add.
Therefore the B1Add -> B2Add will not happen again.
In practice, we end up with these extra transitions that never happen again once pin knows the correct split of the bbls.

For further reading on how Pin works: https://software.intel.com/sites/landingpage/pintool/docs/71313/Pin/html/
