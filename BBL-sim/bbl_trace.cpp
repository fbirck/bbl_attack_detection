#include <cstdlib>
#include <iostream>
#include <map>
#include <time.h>
#include "pin.H"
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "pinoutput", "specify file prefix for main traces, adds pid as suffixes");
KNOB<UINT32> KnobMode(KNOB_MODE_WRITEONCE, "pintool", "m", "pinoutput", "specify execution mode, fast (0) or safe(1). Default is fast.");

INT32 Usage(){
    PIN_ERROR( "This Pintool outputs a trace of BBLs for each thread\n"
              + KNOB_BASE::StringKnobSummary() + "\n");
    return -1;
}

int num_threads = 0; //number of threads
string fbmname; //file name string
static PIN_LOCK init_lock;
int safemode = 0;

std::map<long signed, std::ofstream*> fbmtracer; //file pointers for multiple PIDs, maps PID -> file pointer

//function that prints BBL first address and number of instructions in it
VOID safeprintbblinfo(UINT32 _numins, ADDRINT _bbladdr, THREADID tid){
	int32_t currpid = PIN_GetPid();	//gets PID
        PIN_GetLock(&init_lock, tid+1); //locks thread

	string buf;
	stringstream pidstring;
       	pidstring << currpid;	
	string statline = "/proc/" + pidstring.str() + "/status";
	std::ifstream aux;
	aux.open(statline.c_str());
	if (aux.is_open()){
		std::getline(aux,buf);	
		aux.close();	
	}
	std::string delim = "Name:  ";
	std::string binname = buf.substr(buf.find(delim)+7,buf.length());
	string tracename = KnobOutputFile.Value() + "_" + binname + "_" + pidstring.str() + ".bbl"; //creates name for new process file
	
	fbmtracer[currpid] = new std::ofstream(tracename.c_str(), std::ofstream::app);
	if (fbmtracer[currpid]->is_open()){
		*fbmtracer[currpid] << _bbladdr << " " << _numins << endl;
		fbmtracer[currpid]->close();
	}
	else {
		LOG("something went awry\n");
	}
	PIN_ReleaseLock(&init_lock); //release lock
}

VOID fastprintbblinfo(UINT32 _numins, ADDRINT _bbladdr, THREADID tid){
	int32_t currpid = PIN_GetPid();	//gets PID
        PIN_GetLock(&init_lock, tid+1); //locks thread

	if (fbmtracer[currpid]->is_open()){
		*fbmtracer[currpid] << _bbladdr << " " << _numins << endl;
	}
	else {
		LOG("something went awry\n");
	}
	PIN_ReleaseLock(&init_lock); //release lock
}
//instrumentation function for every trace, inserts calls to printbblinfo for every BBL
VOID Trace(TRACE trace, VOID *v)
{
		for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)){ //for every BBL in this trace
			//insert call to printbblinfo with parameters numins, BBL_Address and thread id
			BBL_InsertCall(bbl, IPOINT_ANYWHERE, (safemode) ? (AFUNPTR)safeprintbblinfo : (AFUNPTR)fastprintbblinfo , IARG_FAST_ANALYSIS_CALL, 
                        IARG_UINT32, BBL_NumIns(bbl), IARG_ADDRINT, BBL_Address(bbl), IARG_THREAD_ID, IARG_END);
		}
}

//keeps track of forked processes, first method
BOOL ChangeChild(CHILD_PROCESS childProcess, VOID * userData){
	LOG ("entered child call, PID," + decstr(CHILD_PROCESS_GetId(childProcess)) + "\n");
	return TRUE;
}

//keeps track of forked processes, second method, similar to first, called in child processes
VOID ChangeFork(THREADID threadid, const CONTEXT *ctxt, VOID *arg ){
	if (!safemode){
                int32_t currpid = PIN_GetPid(); //gets PID
                string buf;
                stringstream pidstring;
                pidstring << currpid;
                string statline = "/proc/" + pidstring.str() + "/status";
                std::ifstream aux;
                aux.open(statline.c_str());
                if (aux.is_open()){
                        std::getline(aux,buf);
                        aux.close();
                }
                std::string delim = "Name:  ";
                std::string binname = buf.substr(buf.find(delim)+7,buf.length());
                string tracename = KnobOutputFile.Value() + "_" + binname + "_" + pidstring.str() + ".bbl"; //creates name for new process file
                fbmtracer[currpid] = new std::ofstream(tracename.c_str(), std::ofstream::app);
        	cout << "forked " << endl;
	}

	LOG ("entered fork call with pid" + decstr(PIN_GetPid()) + "\n");
        return;
}

VOID Finalize(INT32 Code, VOID *v){
	///iterate through map to deallocate file pointers
	for ( auto _itfiles = fbmtracer.begin(); _itfiles != fbmtracer.end(); _itfiles++)
		if (_itfiles->second->is_open()){
			_itfiles->second->close();
	}
}

static VOID printRTN(RTN rtn, VOID *){
//	cout << "got routine " << RTN_Name(rtn) << " address " << std::hex << RTN_Address(rtn) <<  endl;
	return;
}

int main(int argc, char *argv[])
{
	//init pint
	PIN_InitSymbols();
	PIN_InitLock(&init_lock);

	//usage check
	if( PIN_Init(argc,argv) ) {
	return Usage();
	}

	safemode = KnobMode.Value();

	if (!safemode){
		int32_t currpid = PIN_GetPid();	//gets PID
		string buf;
		stringstream pidstring;
		pidstring << currpid;	
		string statline = "/proc/" + pidstring.str() + "/status";
		std::ifstream aux;
		aux.open(statline.c_str());
		if (aux.is_open()){
			std::getline(aux,buf);	
			aux.close();	
		}
		std::string delim = "Name:  ";
		std::string binname = buf.substr(buf.find(delim)+7,buf.length());
		string tracename = KnobOutputFile.Value() + "_" + binname + "_" + pidstring.str() + ".bbl"; //creates name for new process file
		fbmtracer[currpid] = new std::ofstream(tracename.c_str(), std::ofstream::app);
	}
	//add the instrumentation functions, to be called on pin's dynamic binary translation	
	PIN_AddFollowChildProcessFunction(ChangeChild, 0);
	PIN_AddForkFunction(FPOINT_AFTER_IN_CHILD,ChangeFork, 0);   
	TRACE_AddInstrumentFunction(Trace, 0);
	PIN_AddFiniFunction(Finalize,0);
	RTN_AddInstrumentFunction(printRTN,0);
	//Then run program with changed binary
	PIN_StartProgram();

	return 0;
}
