#!/bin/bash

#renaming script

for i in {1..5}; do
	tracen=1
	ls procmailatk${i}/*bbl | awk -F _ '{print $NF,$0}' | sort -k 1 -t '_' > list;
	while IFS='' read -r line || [[ -n "$line" ]]; do
		fname=$(echo $line | awk '{print $2}');
		mv $fname procmailatk${i}/procatk${i}_${tracen};
		tracen=$((tracen + 1))
	done < list
done;
