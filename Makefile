all: CBBT-process/cbbt_generate STAT-testing/fastcheckermax feature-training/bbl_simulate CFG-research/CFGtool/cfg_gen

CBBT-process/cbbt_generate:
	cd CBBT-process; \
	make; \

STAT-testing/fastcheckermax:
	cd STAT-testing; \
	make; \

feature-training/bbl_simulate:
	cd feature-training; \
	make; \

CFG-research/CFGtool/cfg_gen:
	cd CFG-research; \
	make; \

clean:
	cd CBBT-process; \
	make clean; \
	cd ../STAT-testing; \
	make clean; \
	cd ../CFG-research; \
	make clean;
